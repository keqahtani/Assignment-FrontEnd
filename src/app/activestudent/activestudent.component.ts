import { Component, OnInit } from '@angular/core';
import {Student} from '../student.model';
import {StudentService} from '../student.service';


@Component({
  selector: 'app-activestudent',
  templateUrl: './activestudent.component.html',
  styleUrls: ['./activestudent.component.css']
})
export class ActivestudentComponent implements OnInit {
  student$: Student[];
  p = 1;


  constructor(private student: StudentService) { }

  ngOnInit() {
    this.studentActive();
  }




  deleteStudent(sid: number) {
    this.student.deleteStudent(sid).subscribe(del => this.ngOnInit(),
    );
  }

  studentActive() {
    this.student.studentActive().subscribe(res => {
        // studentActive = this.student$;
        this.student$ = res;
        res[0];  
        console.log(res[0]);
              
      },
      err => console.log(err),
    );
  }
  

}
